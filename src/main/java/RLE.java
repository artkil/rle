public class RLE {

    public static String rle(String value) throws Exception
    {
        if (value == null) {
            throw new Exception("Null value");
        }

        if ("".equals(value)) {
            throw new Exception("Empty value");
        }

        if (value.length() == 1) {
            return value;
        }

        long counter = 1;
        StringBuilder result = new StringBuilder();

        for (int i = 1; i < value.length(); i++) {
            char prevChar = value.charAt(i - 1);
            char currentChar = value.charAt(i);

            if (!('A' <= prevChar && prevChar <= 'Z')
                    || !('A' <= currentChar && currentChar <= 'Z')) {
                throw new Exception("Illegal character");
            }

            if (prevChar != currentChar) {
                if (counter == 1) {
                    result.append(prevChar);
                } else {
                    result.append(prevChar).append(counter);
                }

                counter = 0L;
            }

            counter += 1L;

            if (i == value.length() - 1) {
                if (counter == 1) {
                    result.append(currentChar);
                } else {
                    result.append(currentChar).append(counter);
                }
            }
        }

        return result.toString();
    }
}
