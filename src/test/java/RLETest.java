import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class RLETest {
    @Test
    public void expectException1()
    {
        assertThrows(Exception.class, () -> {
            RLE.rle(null);
        });
    }

    @Test
    public void expectException2()
    {
        assertThrows(Exception.class, () -> {
            RLE.rle("");
        });
    }

    @Test
    public void expectException3()
    {
        assertThrows(Exception.class, () -> {
            RLE.rle("A ");
        });
    }

    @Test
    public void test1() throws Exception
    {
        String inValue = "A";
        String expected = "A";
        String outValue = RLE.rle(inValue);
        assertEquals(expected, outValue);
    }

    @Test
    public void test2() throws Exception
    {
        String inValue = "AA";
        String expected = "A2";
        String outValue = RLE.rle(inValue);
        assertEquals(expected, outValue);
    }

    @Test
    public void test3() throws Exception
    {
        String inValue = "AAB";
        String expected = "A2B";
        String outValue = RLE.rle(inValue);
        assertEquals(expected, outValue);
    }

    @Test
    public void test4() throws Exception
    {
        String inValue = "AABB";
        String expected = "A2B2";
        String outValue = RLE.rle(inValue);
        assertEquals(expected, outValue);
    }

    @Test
    public void test5() throws Exception
    {
        String inValue = "AABA";
        String expected = "A2BA";
        String outValue = RLE.rle(inValue);
        assertEquals(expected, outValue);
    }

    @Test
    public void test6() throws Exception
    {
        String inValue = "AABBA";
        String expected = "A2B2A";
        String outValue = RLE.rle(inValue);
        assertEquals(expected, outValue);
    }

    @Test
    public void test7() throws Exception
    {
        String inValue = "AABBAA";
        String expected = "A2B2A2";
        String outValue = RLE.rle(inValue);
        assertEquals(expected, outValue);
    }

    @Test
    public void test8() throws Exception
    {
        String inValue = "AB";
        String expected = "AB";
        String outValue = RLE.rle(inValue);
        assertEquals(expected, outValue);
    }

    @Test
    public void test9() throws Exception
    {
        String inValue = "ABB";
        String expected = "AB2";
        String outValue = RLE.rle(inValue);
        assertEquals(expected, outValue);
    }
}
